#    Minesweeper
#   Copyright (C) 2019 Cameron Himes (GitLab username: Government Experiment 6502-CATON)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

from enum import Enum
from random import randrange
from sys import argv

# options
printDebug = False
board_size = [22,22] # [rows,columns]
mine_count = 10
userPos = [0,0] # [x,y]

class TileType(Enum):
    EMPTY = " "
    ONE = "1"
    TWO = "2"
    THREE = "3"
    FOUR = "4"
    FIVE = "5"
    SIX = "6"
    SEVEN = "7"
    EIGHT = "8"
    UNKNOWN = "?"
    MINE = "*"
    FLAG = "!"
    ERROR = "@"
    NULL = "N"
    SELECTED = "#"

class Tile(object):
    dna = TileType.NULL
    isHidden = True
    isFlagged = False
    isSelected = False
    def __init__(self,initDNA=TileType.NULL):
        self.dna = initDNA
    def hide(self):
        self.isHidden = True
    def show(self):
        self.isHidden = False
        self.unflag()
    def select(self):
        self.isSelected = True
    def deselect(self):
        self.isSelected = False
    def flagged(self):
        return self.isFlagged
    def flag(self):
        if self.isHidden:
            self.isFlagged = True
    def unflag(self):
        self.isFlagged = False
    def toggleFlag(self):
        if self.isHidden:
            if self.isFlagged:
                self.unflag()
            else:
                self.flag()
    def getChar(self, override=False):
        if self.isSelected and not override:
            return TileType.SELECTED.value
        elif self.isFlagged and not override:
            return TileType.FLAG.value
        elif self.isHidden and not override:
            return TileType.UNKNOWN.value
        else:
            return self.dna.value
    def getName(self):
        return self.dna.name
    def setDNA(self, newDNA):
        self.dna = newDNA
    def getDNA(self):
        return self.dna
    def equals(self, compare):
        if self.dna == compare:
            return True
        else:
            return False

def generateBoard(sizeX,sizeY, mineCount):
    # declare 2d list
    board = []
    # made board of empty tiles
    for i in range(sizeY):
        row = board = [[Tile(TileType.EMPTY) for _ in range(sizeX)] for _ in range(sizeY)]
    # add mines
    for i in range(mineCount):
        mineX = randrange(0,sizeX)
        mineY = randrange(0,sizeY)
        # check if mine is already present
        while board[mineY][mineX].equals(TileType.MINE):
            # get better coords
            mineX = randrange(0,sizeX)
            mineY = randrange(0,sizeY)
        else:
            # when loop breaks, set the mine
            board[mineY][mineX].setDNA(TileType.MINE)
        if printDebug: print("Mine plotted at (%s,%s)" % (mineX,mineY))
    # iterate every space and add number values
    for y in range(sizeY):
        for x in range(sizeX):
            if printDebug: print("Checking surroundings at (%s,%s)" % (x,y))
            if board[y][x].equals(TileType.MINE):
                # if the space is a mine, just pass
                if printDebug: print("Space is a mine. Aborting loop iteration.")
                continue
            # detect neighbors
            hits = 0
            # if you plot testY and testX, it's just a 3x3 grid around (x,y)
            for testY in range(y-1,y+2):
                for testX in range(x-1,x+2):
                    if printDebug: print("Checking (%s,%s) around (%s,%s)." % (testX,testY,x,y))
                    # do not check values that are off the grid
                    if (testX < 0) or (testY < 0) or (testX >= sizeX) or (testY >= sizeY):
                        continue
                    # do not check (x,y) since we check SURROUNDING tiles
                    if (testX == x) and (testY == y):
                        if printDebug: print("Position is the middle of the grid. Aborting loop iteration")
                        continue
                    # check position for a mine
                    if board[testY][testX].equals(TileType.MINE):
                        # increment counter
                        hits += 1
                        if printDebug: print("Found mine when checking relative to (%s,%s)" % (x,y))
                        if printDebug: print("Mine found. Counter is now at %s" % (hits))
            # check hits value for proper tile assignment
            if hits == 0:
                board[y][x].setDNA(TileType.EMPTY)
                if printDebug: print("Changing DNA to %s at (%s,%s)" % (TileType.EMPTY.name,x,y))
            elif hits == 1:
                board[y][x].setDNA(TileType.ONE)
                if printDebug: print("Changing DNA to %s at (%s,%s)" % (TileType.ONE.name,x,y))
            elif hits == 2:
                board[y][x].setDNA(TileType.TWO)
                if printDebug: print("Changing DNA to %s at (%s,%s)" % (TileType.TWO.name,x,y))
            elif hits == 3:
                board[y][x].setDNA(TileType.THREE)
                if printDebug: print("Changing DNA to %s at (%s,%s)" % (TileType.THREE.name,x,y))
            elif hits == 4:
                board[y][x].setDNA(TileType.FOUR)
                if printDebug: print("Changing DNA to %s at (%s,%s)" % (TileType.FOUR.name,x,y))
            elif hits == 5:
                board[y][x].setDNA(TileType.FIVE)
                if printDebug: print("Changing DNA to %s at (%s,%s)" % (TileType.FIVE.name,x,y))
            elif hits == 6:
                board[y][x].setDNA(TileType.SIX)
                if printDebug: print("Changing DNA to %s at (%s,%s)" % (TileType.SIX.name,x,y))
            elif hits == 7:
                board[y][x].setDNA(TileType.SEVEN)
                if printDebug: print("Changing DNA to %s at (%s,%s)" % (TileType.SEVEN.name,x,y))
            elif hits == 8:
                board[y][x].setDNA(TileType.EIGHT)
                if printDebug: print("Changing DNA to %s at (%s,%s)" % (TileType.EIGHT.name,x,y))
            else:
                board[y][x].setDNA(TileType.ERROR)
                if printDebug: print("Changing DNA to %s at (%s,%s)" % (TileType.ERROR.name,x,y))
    # return results
    return board

def getSurroundingTiles(coords):
    neighbors = []
    for r in range(coords[0]-1,coords[0]+2):
        for c in range(coords[1]-1,coords[1]+2):
            # TODO: check if valid
            if (r < 0) or (r > board_size[0]-3) or (c < 0) or (c > board_size[1]-3):
                continue
            neighbors.append([r,c])
    return neighbors

def expose_tiles(grid):
    # get the user's position
    global userPos
    # make list for already scanned tiles
    scanned = [[userPos[1],userPos[0]]]
    # expose current tile
    grid[userPos[1]][userPos[0]].show()
    # make queue
    queue = getSurroundingTiles([userPos[1],userPos[0]])
    # keep checking surrounding tiles
    while True:
        # make new queue
        nextQueue = []
        # check every tile in the queue
        for pos in queue:
            # check if it is already scanned
            if not [pos[0],pos[1]] in scanned:
                # check all around empty spaces
                if grid[pos[0]][pos[1]].equals(TileType.EMPTY):
                    # show tile
                    grid[pos[0]][pos[1]].show()
                    scanned.append([pos[0],pos[1]])
                    for element in getSurroundingTiles([pos[0],pos[1]]):
                        nextQueue.append(element)
                # check for numbers and expose them
                elif grid[pos[0]][pos[1]].equals(TileType.ONE):
                    grid[pos[0]][pos[1]].show()
                    scanned.append([pos[0],pos[1]])
                elif grid[pos[0]][pos[1]].equals(TileType.TWO):
                    grid[pos[0]][pos[1]].show()
                    scanned.append([pos[0],pos[1]])
                elif grid[pos[0]][pos[1]].equals(TileType.THREE):
                    grid[pos[0]][pos[1]].show()
                    scanned.append([pos[0],pos[1]])
                elif grid[pos[0]][pos[1]].equals(TileType.FOUR):
                    grid[pos[0]][pos[1]].show()
                    scanned.append([pos[0],pos[1]])
                elif grid[pos[0]][pos[1]].equals(TileType.FIVE):
                    grid[pos[0]][pos[1]].show()
                    scanned.append([pos[0],pos[1]])
                elif grid[pos[0]][pos[1]].equals(TileType.SIX):
                    grid[pos[0]][pos[1]].show()
                    scanned.append([pos[0],pos[1]])
                elif grid[pos[0]][pos[1]].equals(TileType.SEVEN):
                    grid[pos[0]][pos[1]].show()
                    scanned.append([pos[0],pos[1]])
                elif grid[pos[0]][pos[1]].equals(TileType.EIGHT):
                    grid[pos[0]][pos[1]].show()
                    scanned.append([pos[0],pos[1]])
        # overwrite queue
        queue = nextQueue
        # check if queue is empty
        if len(nextQueue) == 0:
            break

def drawBoard(grid, override=False):
    # make top like
    for i in range(board_size[1]):
        print("~",end='')
    # shift to new line
    print()
    # draw board
    for row in grid:
        print("~",end="")
        for tile in row:
            print(tile.getChar(override),end="")
        print("~")
    # make bottom line
    for i in range(board_size[1]):
        print("~",end='')
    # shift to new line
    print()

def endGame(didWin, exitOverride=False):
    if exitOverride:
        print("Giving up this early?")
    elif didWin:
        print("You win!")
    else:
        print("You died!")
    exit()

def moveUser(grid, direction):
    global userPos
    # deselect current position
    grid[userPos[1]][userPos[0]].deselect()
    # check direction
    if direction == "HOME":
        userPos = [0,0]
    elif direction == "UP":
        if userPos[1]-1 >= 0 and userPos[1]-1 < board_size[0]:
            userPos[1] -= 1
    elif direction == "DOWN":
        if userPos[1]+1 >= 0 and userPos[1]+1 < board_size[0]-2:
            userPos[1] += 1
    elif direction == "LEFT":
        if userPos[0]-1 >= 0 and userPos[0]-1 < board_size[1]:
            userPos[0] -= 1
    elif direction == "RIGHT":
        if userPos[0]+1 >= 0 and userPos[0]+1 < board_size[1]-2:
            userPos[0] += 1
    # select current position
    grid[userPos[1]][userPos[0]].select()

def selectTile(grid):
    global userPos
    # define behavior based on tile DNA
    if grid[userPos[1]][userPos[0]].equals(TileType.EMPTY):
        expose_tiles(grid)
    elif grid[userPos[1]][userPos[0]].equals(TileType.ONE):
        grid[userPos[1]][userPos[0]].show()
    elif grid[userPos[1]][userPos[0]].equals(TileType.TWO):
        grid[userPos[1]][userPos[0]].show()
    elif grid[userPos[1]][userPos[0]].equals(TileType.THREE):
        grid[userPos[1]][userPos[0]].show()
    elif grid[userPos[1]][userPos[0]].equals(TileType.FOUR):
        grid[userPos[1]][userPos[0]].show()
    elif grid[userPos[1]][userPos[0]].equals(TileType.FIVE):
        grid[userPos[1]][userPos[0]].show()
    elif grid[userPos[1]][userPos[0]].equals(TileType.SIX):
        grid[userPos[1]][userPos[0]].show()
    elif grid[userPos[1]][userPos[0]].equals(TileType.SEVEN):
        grid[userPos[1]][userPos[0]].show()
    elif grid[userPos[1]][userPos[0]].equals(TileType.EIGHT):
        grid[userPos[1]][userPos[0]].show()
    elif grid[userPos[1]][userPos[0]].equals(TileType.MINE):
        endGame(False)

def checkWin(grid):
    # make flag for unknown mines
    didWin = True
    # iterate over all tiles
    for row in grid:
        for char in row:
            # check if mine and not flagged
            if char.equals(TileType.MINE) and not char.flagged():
                # the user did not win, set the flag to false
                didWin = False
                break
            # all other spaces should not be flagged
            elif not char.equals(TileType.MINE) and char.flagged():
                didWin = False 
                break
        # stop looping if flag changed
        if not didWin:
            break
    # return result
    return didWin

def main():
    # make board
    grid = generateBoard(board_size[1]-2,board_size[0]-2,mine_count)
    # place user
    moveUser(grid, "HOME")
    # enter main loop
    while True:
        # draw board
        drawBoard(grid)
        # prompt user
        ans = input("Enter command: ").lower()
        if ans == "quit" or ans == "exit":
            endGame(False,True)
        elif ans == "cheat":
            drawBoard(grid, True)
            input("Press ENTER to return to main loop.")
        elif ans == "up" or ans == "w":
            moveUser(grid, "UP")
        elif ans == "down" or ans == "s":
            moveUser(grid, "DOWN")
        elif ans == "left" or ans == "a":
            moveUser(grid, "LEFT")
        elif ans == "right" or ans == "d":
            moveUser(grid, "RIGHT")
        elif ans == "home":
            moveUser(grid, "HOME")
        elif ans == "flag":
            grid[userPos[1]][userPos[0]].toggleFlag()
        elif ans == "select":
            selectTile(grid)
        elif ans == "show c":
            showGPL3Conditions()
        elif ans == "show w":
            showGPL3Warranty()
        else:
            print("Command \"%s\" failed." % (ans))
        # check if the user won
        if checkWin(grid):
            endGame(True)

def showGPL3():
    print("""Minesweeper  Copyright (C) 2019  Cameron Himes
This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.
This is free software, and you are welcome to redistribute it
under certain conditions; type `show c' for details.
""")

def showGPL3Conditions():
    print("""This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
""")

def showGPL3Warranty():
    print("""This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
    """)

def setupPrompt():
    global board_size
    global mine_count
    try:
        board_size = [int(input("Enter board height: ")), int(input("Enter board width: "))]
        mine_count = int(input("Enter number of mines: "))
    except:
        print("Entered value must be numeric.")
        exit()

def setupParseArgs():
    global board_size
    global mine_count
    try:
        board_size = [int(argv[1]), int(argv[2])]
        mine_count = int(argv[3])
    except:
        print("Provided arguements could not be parsed. Prompting manually...")
        setupPrompt()

def setup():
    global board_size
    global mine_count
    # get args if present
    if len(argv) == 4:
        setupParseArgs()
    else:
        setupPrompt()
    # ensure board values are more than two
    if (board_size[0] < 5) or (board_size[1] < 5):
        print("Board values must be 5 or greater.")
        exit()
    # ensure mine count is less than the board's area
    if mine_count >= (board_size[0]-2) * (board_size[1]-2):
        print("There are more mines than board space.")
        exit()

if __name__ == "__main__":
    try:
        showGPL3()
        setup()
        main()
    except KeyboardInterrupt:
        endGame(False,True)
