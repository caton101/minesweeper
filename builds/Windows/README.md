# Minesweeper

This is my port of Minesweeper for the terminal. It was made as part of a challenge on my Discord server.

![screenshot](Screenshot.png)

## Usage

`python3 src/minesweeper.py`

## Commands

Here are all the in-game commands:

| Command | Description             |
| :-----: | :---------------------- |
| up      | move cursor up          |
| down    | move cursor down        |
| left    | move cursor left        |
| right   | move cursor right       |
| w       | move cursor up          |
| a       | move cursor left        |
| s       | move cursor down        |
| d       | move cursor right       |
| home    | move cursor to top left |
| flag    | flag the selected tile  |
| select  | show the selected tile  |
| quit    | exit the game           |
| exit    | exit the game           |
| cheat   | peek at all tiles       |
| show w  | show the GPL3 license   |
| show c  | show license conditions |
