@echo off

rem determine processor architecture
if %PROCESSOR_ARCHITECTURE%==AMD64 (
    goto run32
) else (
    goto run64
)

:run32
rem launch 32 bit version
echo "Running in 32 bit mode."
"./python-3.8.3-embed-win32/python.exe" "./minesweeper.py"
goto halt

:run64
rem launch 64 bit version
echo "Running in 64 bit mode."
"./python-3.8.3-embed-amd64/python.exe" "./minesweeper.py"
goto halt

:halt
rem exit the shell script
exit